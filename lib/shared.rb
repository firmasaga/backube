require 'yaml'

module Backube
  def namespaces
    %w(development staging production)
  end

  class RC
    def initialize(config)
      @config = YAML.load(config)
      @containers = containers
      @name = name
      @volumes = volumes
      @namespace = namespace
      @host = host
    end

    def svcs
      @containers.map { |container| container.config }
    end

    def rc
      ERB.new(File.read('templates/rc.yml.erb'), nil, '-').result(binding)
    end

    private
    def name
      @config['name']
    end

    def host
      @config['host']
    end

    def namespace
      @config['namespace']
    end

    def volumes
      @containers.map { |container| container.volumes }.flatten.uniq { |volume| volume.hostPath }
    end

    def containers
      @config['containers'].map do |config|
        config['namespace'] = namespace
        Container.new(config)
      end
    end

  end

  class Container

    def initialize(config)
      @config = config
      @name = name
      @namespace = namespace
      @ports = ports
      @target_ports = target_ports
      @domain = domain
    end

    def config
      ERB.new(File.read('templates/svc.yml.erb'), nil, '-').result(binding)
    end

    def volumes
      @config['volumes'].map { |config| Volume.new(config) }
    end

    def name
      @config['name']
    end

    def namespace
      @config['namespace']
    end

    def working_dir
      @config['workingDir']
    end

    def command
      @config['command']
    end

    def domain
      @config['domain']
    end

    def ports
      @config['ports'].map { |config| Port.new(config) }
    end

    def target_ports
      @config['ports'].map { |config| Port.new(config) }.uniq {|port| port.target_port}
    end

    def variables
      read_variables(@config['envFile'])
    end

    def image
      @config['image']
    end

    private
    def read_variables(env_file)
      env_file ? File.readlines(env_file) : []
    end
  end

  class Port
    def initialize(config)
      @config = config
    end

    def protocol
      'TCP'
    end

    def port
      @config.to_s.split(':').first
    end

    def target_port
      @config.to_s.split(':').last
    end

  end
  class Volume
    def initialize(config)
      @config = config
    end

    def path
      @config['path']
    end

    def hostPath
      @config['hostPath']
    end

    def name
      @config['path'].gsub('/', '_')
    end

  end
end
